defmodule TimexWeb.IndigloManager do
  use GenServer
  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {:ok, %{ui_pid: ui, st: IndigloOff, ct: 0}}
  end

  def handle_info(:"top-right", %{st: IndigloOff, ui_pid: ui} = state) do
    GenServer.cast(ui, :set_indiglo)
    {:noreply, %{state | st: IndigloOn} }
  end

  def handle_info(:"top-right", %{st: IndigloOn} = state) do
    Process.send_after(self(), Waiting2IndigloOff, 2000)
    {:noreply, %{state | st: Waiting} }
  end

  def handle_info(Waiting2IndigloOff, %{st: Waiting, ui_pid: ui} = state) do
    GenServer.cast(ui, :unset_indiglo)
    {:noreply, state |> Map.put(:st, IndigloOff)}
  end

  def handle_info(:alarm, %{st: st1} = state) do
    IO.puts("Entro alarma")
    if st1 == IndigloOff do
      Process.send_after(self(), :alarm_off, 1000)
    else
      Process.send_after(self(), :alarm_on, 1000)
    end
    {:noreply, state}
  end

   def handle_info(:alarm_off, %{st: IndigloOff, ui_pid: ui, ct: counter} = state) when counter < 10 do
     IO.puts("Apago luz")
     Process.send_after(self(), :alarm_on, 1000)
     GenServer.cast(ui, :set_indiglo)
     {:noreply, state |> Map.put(:ct, counter + 1) |> Map.put(:st, IndigloOn)}
   end

   def handle_info(:alarm_on, %{st: IndigloOn, ui_pid: ui, ct: counter} = state) do
     IO.puts("Enciendo luz")
     GenServer.cast(ui, :unset_indiglo)
     if(counter != 10) do
       Process.send_after(self(), :alarm_off, 1000)
       {:noreply, state |> Map.put(:st, IndigloOff) |> Map.put(:ct, counter + 1)}
     else
       {:noreply, state |> Map.put(:st, IndigloOff) |> Map.put(:ct, 0)}
     end
   end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
